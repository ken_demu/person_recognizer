# person_recognizer

Source Code used in RoboCup@Home 2016/2017 Person Recognition  

## License
BSD-3  

```git clone --recursive https://github.com/demulab/person_recognizer.git```

## dependencies  
CUDA 7.5 runtime  
opencv(GPU support build)  
caffe/pycaffe  
ROS  
sqlite3  
h5py  
openface  
gstreamer 1.0  
sound_play  
## face_detector(deprecated)  
GPU Haarcascade face detector used in RoboCup 2016.
We are currently using YOLOv2 human detector.
[yolo9000_tensorflow](https://github.com/kendemu/yolo9000_tensorflow)  
  
## ROS Msg  
###Face  
Header header  
uint8 number  
sensor_msgs/Image[] faces  
int32[] xangle  
int32[] yangle  
## gender_recognizer.py  
gender recognition with Caffe CNN.  
## person_recognizer_server.py  
person memorizing and matching with openface.  
## name_extractor.py  
name extraction from voice using kaldi and nltk.  
## person_recognizer_client.py  
person memorizing, matching and describing features of people.  